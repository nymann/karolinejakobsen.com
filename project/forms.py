from flask_babelplus import gettext
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, MultipleFileField, FileField, TextField, DateField
from wtforms.validators import DataRequired, Email, Length


class LoginForm(FlaskForm):
    email = StringField(label='email', validators=[DataRequired(), Email()])
    u = gettext("Password should minimum be 8 characters, and maximum 100 characters.")
    password = StringField(
        label='password',
        validators=[
            Length(min=8, max=100, message=u)
        ]
    )

    def __repr__(self):
        return f"email: {self.email.data}\npassword: {self.password.data}"


class LoginRememberMeForm(LoginForm):
    remember = BooleanField(label='remember')

    def __repr__(self):
        return super(LoginRememberMeForm, self).__repr__() + "\nremember: {self.remember.data}"


class ProjectForm(FlaskForm):
    title = StringField(label='title', validators=[DataRequired()])
    description = StringField(label='description')
    pictures = MultipleFileField(label='pictures')
    cover_picture = FileField(label='cover_picture')
    date = StringField(label="date")
