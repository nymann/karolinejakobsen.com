from flask import render_template, redirect, url_for

from project.forms import ProjectForm
from project.models import Project, Picture, db, commit, FrontPage
from project.site import site
from project.utilities import flash_errors
from project.utilities.decorators import is_admin


@site.before_app_first_request
def insert_front_page_settings():
    if FrontPage.query.count() == 0:
        front_page = FrontPage()
        front_page.store()


@site.route('/')
def index():
    front_page = FrontPage.query.get(1)
    batch_length = 4 if len(front_page.pictures) % 2 == 0 else 3

    return render_template('site/index.html', pictures=front_page.pictures, batch_length=batch_length)


@site.route('/projects')
def projects():
    projects = Project.query.all()
    return render_template('site/projects.html', projects=projects, batch_length=3)


@site.route("/projects/<int:project_id>/remove/photo/<string:filename>")
def remove_photo_from_project(project_id, filename):
    project = Project.query.get_or_404(project_id)
    picture = Picture.query.get_or_404(filename)
    project.pictures.remove(picture)
    commit()
    return redirect(url_for("site.project", id=project.id))


@site.route("/projects/<int:id>")
def project(id):
    project = Project.query.get(id)
    pictures = db.session.query(
        Picture.filename.label("filename")
    ).filter(
        Picture.filename != project.cover_picture,
        Picture.project_id == project.id
    ).all()
    return render_template("site/project.html", pictures=pictures, project=project)


@site.route('/projects/new', methods=['GET', 'POST'])
@is_admin
def new_project():
    form = ProjectForm()
    if form.validate_on_submit():
        # Save the project to the db.
        project = Project(form)
        project.store()

        # Save all the uploaded photos.
        project.store_photos(form)

    flash_errors(form)

    return render_template("site/new.html", form=form)


@site.route("/projects/<int:id>/delete")
@is_admin
def delete_project(id):
    project = Project.query.get(id)
    project.remove_photos()

    db.session.delete(project)
    commit()

    Picture.query.filter(
        Picture.project_id == id
    ).delete()
    commit()

    return redirect(url_for("site.projects"))


@site.route('/error')
def error():
    return render_template('errors/internal_server_error.html')


@site.route("/about")
def about():
    return render_template("site/about.html")
