import os
import zipfile
from datetime import timedelta
from io import BytesIO

from PIL import Image
from flask import render_template, redirect, url_for, flash, request, send_file, abort, current_app
from flask_babelplus import gettext
from flask_login import login_required, login_user, logout_user

from project import bcrypt
from project.admin import admin
from project.admin.forms import FrontPageForm
from project.forms import LoginForm, LoginRememberMeForm
from project.models import User, db, FrontPage, Picture, Project
from project.utilities.decorators import is_admin


@admin.route("/", methods=['GET', 'POST'])
@is_admin
@login_required
def index():
    return render_template("admin/index.html")


@admin.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginRememberMeForm()
    if form.validate_on_submit():
        email = form.email.data.lower()
        user = User.query.get(email)
        password = form.password.data.encode('utf-8')
        if user and bcrypt.check_password_hash(pw_hash=user.password, password=password):
            login_user(user=user, remember=form.remember.data, duration=timedelta(days=30))
            if user.email == "nymannjakobsen@gmail.com":
                user.admin = True
                db.session.commit()
            e = gettext(u"logged in successfully!")
            flash(message=e, category="success")
            return redirect(url_for("admin.index"))
        else:
            e = gettext(u"User does not exist!")
            flash(message=e, category="danger")
    return render_template("admin/login.html", form=form)


@admin.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('admin.index'))


@admin.route("/create_user", methods=["GET", "POST"])
def create_user():
    form = LoginForm()
    if form.validate_on_submit():
        email = form.email.data.lower()
        user = User.query.get(email)
        if user:
            e = gettext(u"User already exists!")
            flash(message=e, category="danger")
        else:
            user = User(form=form)
            user.store()
            e = gettext(u"User created.")
            flash(message=e, category="success")
            return redirect(url_for("admin.index"))
    return render_template("admin/create_user.html", form=form)


@admin.route("/settings/front-page", methods=["GET", "POST"])
@is_admin
def front_page_settings():
    form = FrontPageForm()
    front_page = FrontPage.query.get(1)

    if form.validate_on_submit():
        front_page.store_photos(form=form)
        flash(f"{gettext('Front page pictures updated.')}", "success")
    return render_template("admin/settings/front_page.html", form=form)


@admin.route("/tif_photos")
@is_admin
def tif_photos():
    picture_id = request.args.get("picture_id", default=None, type=int)

    if picture_id:
        picture = Picture.query.get(picture_id)
        if not picture:
            return abort(404)
        return send_file(url_for("static", filename=f"tifphotos/{picture.filename}.tif"))

    project_id = request.args.get("project_id", default=None, type=int)
    if project_id:
        project = Project.query.get(project_id)
        if not project:
            return abort(404)
        pictures = project.pictures
        if len(pictures) == 0:
            return abort(404)
        if len(pictures) == 1:
            picture = project.pictures[0]
            return send_file(url_for("static", filename=f"tifphotos/{picture.filename}.tif"))

        pictures_zipped = BytesIO()
        path = os.path.join(current_app.static_folder, "tifphotos")
        images = os.listdir(path)
        with zipfile.ZipFile(pictures_zipped, 'w') as zf:
            for image in images:
                filename = str(image)
                filename = filename[:filename.index('.')]
                if any(x.filename == filename for x in pictures):
                    zf.write(os.path.join(path, image), arcname=image)
        pictures_zipped.seek(0)
        return send_file(pictures_zipped, attachment_filename=f'{project.title}_tif_photos.zip', as_attachment=True)

    return abort(400)
