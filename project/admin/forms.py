from flask_wtf import FlaskForm
from wtforms import MultipleFileField


class FrontPageForm(FlaskForm):
    pictures = MultipleFileField(label='pictures')
