import base64
import os
import uuid

from PIL import Image
from flask_uploads import UploadSet, IMAGES

photos = UploadSet('photos', IMAGES)
tif_photos = UploadSet('tifphotos', IMAGES)
cover_photos = UploadSet('coverphotos', IMAGES)


def process_cover_photo(b64string, upload_set):
    s1 = str(b64string)
    s = s1.partition(",")[2]
    # ext = s1[s1.find("/") + 1:s1.find(";")]
    pad = len(s) % 4
    s += "=" * pad
    imgdata = base64.b64decode(s.strip())
    filename = random_filename()

    dir_path = upload_set.config.destination
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)

    path = os.path.join(dir_path, filename)

    with open(path, 'wb') as f:
        f.write(imgdata)

    return filename


def process_project_photo(file_storage):
    image = open_image(file_storage)
    filename = str(uuid.uuid4())

    # Save the raw TIF file.
    save_image(image=image, upload_set=tif_photos, ext="tif", filename=filename)

    # Resize the image, and save it.
    image = resize_image(image=image, size_ratio=0.25)
    save_image(image=image, upload_set=photos, ext="jpeg", filename=filename)

    return filename


def open_image(file_storage):
    image = Image.open(file_storage.stream)

    if image.format == 'JPEG' and image.mode != "RGB":
        image = image.convert("RGB")

    return image


def save_image(image, upload_set, ext, filename=None):
    filename = f"{filename}.{ext}" if filename else random_filename(ext=ext)
    dir_path = upload_set.config.destination

    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)

    path = os.path.join(dir_path, filename)
    image.save(path)

    return filename


def random_filename(ext="jpeg"):
    return f"{str(uuid.uuid4())}.{ext}"


def resize_image(image, size_ratio=0.5):
    w, h = image.size
    w = int(w * size_ratio)
    h = int(h * size_ratio)
    return image.resize((w, h), Image.ANTIALIAS)
