import sentry_sdk
from flask import Flask, request
from flask_babelplus import Babel, gettext
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_uploads import configure_uploads, UploadSet, IMAGES
from sentry_sdk.integrations.flask import FlaskIntegration

from config import ProductionConfig
from project.utilities.encoder import CustomJSONEncoder
from project.utilities.error_handlers import register_handlers
from project.utilities.upload_sets import photos, tif_photos, cover_photos

login_manager = LoginManager()
login_manager.login_view = "admin.login"
babel = Babel()
bcrypt = Bcrypt()
migrate = Migrate()


def create_app(cfg=None):
    app = Flask(__name__)
    app.json_encoder = CustomJSONEncoder
    if not cfg:
        cfg = ProductionConfig
    app.config.from_object(cfg)
    initialize_extensions(app=app)
    register_blueprints(app=app)
    register_handlers(app=app)
    return app


def initialize_extensions(app):
    # Database
    from project.models import db
    db.init_app(app=app)
    with app.app_context():
        db.create_all(app=app)

    # Migrate
    migrate.init_app(app=app, db=db)

    # Babel
    babel.init_app(app=app)

    @babel.localeselector
    def get_locale():
        return request.accept_languages.best_match(['da', 'en'])

    # Login
    login_manager.login_message = gettext("You need to be authenticated to visit that page.")
    login_manager.login_message_category = "info"
    login_manager.init_app(app=app)
    from project.models import User

    @login_manager.user_loader
    def user_loader(email):
        return User.query.get(email)

    bcrypt.init_app(app=app)

    # Sentry
    sentry_sdk.init(
        integrations=[FlaskIntegration()],
        release="1.0.0"
    )

    # Flask Uploads
    configure_uploads(app=app, upload_sets=(photos, tif_photos, cover_photos))


def register_blueprints(app):
    from project.site import site
    app.register_blueprint(site)

    from project.admin import admin
    app.register_blueprint(admin)
