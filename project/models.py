import os
from datetime import datetime

from flask import flash
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError

from project import bcrypt
from project.utilities.upload_sets import process_project_photo, cover_photos, photos, tif_photos

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True

    def store(self):
        db.session.add(self)
        commit()


def commit(alert_message=None):
    try:
        db.session.commit()
    except SQLAlchemyError as e:
        flash(alert_message if alert_message else e, "danger")
        db.session.rollback()


class User(BaseModel, UserMixin):
    __tablename__ = 'user'
    email = db.Column(db.String, primary_key=True)
    admin = db.Column(db.Boolean, default=False)
    password = db.Column(db.String, nullable=False)

    def get_id(self):
        return self.email

    def __init__(self, form):
        self.email = form.email.data.lower()
        password = form.password.data.encode('utf-8')
        self.password = bcrypt.generate_password_hash(password=password, rounds=10).decode('utf-8')
        self.admin = False


class Picture(BaseModel):
    __tablename__ = 'picture'
    filename = db.Column(db.String, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"))
    front_page_id = db.Column(db.Integer, db.ForeignKey("front_page.id"))
    upload_date = db.Column(db.DateTime, default=datetime.now())

    def __init__(self, filename, project_id=None, front_page_id=None):
        self.filename = process_project_photo(filename)
        self.project_id = project_id
        self.front_page_id = front_page_id

    def update(self, filename, project_id=None):
        self.__init__(filename=filename, project_id=project_id)
        commit()

    def __repr__(self):
        return f"{self.filename}"


class Project(BaseModel):
    __tablename__ = 'project'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, default=datetime.now().date())
    title = db.Column(db.String)
    description = db.Column(db.Text)
    pictures = db.relationship("Picture", foreign_keys="Picture.project_id")
    cover_picture = db.Column(db.String, db.ForeignKey("picture.filename"))

    def __init__(self, form):
        self.date = datetime.strptime(form.date.data, "%d/%m/%Y").date() if form.date.data else datetime.now().date()
        self.title = form.title.data
        self.description = form.description.data

    def store_photos(self, form):
        if form.cover_picture.data:
            cover_picture = Picture(filename=form.cover_picture.data, project_id=self.id)
            cover_picture.store()
            self.cover_picture = cover_picture.filename
            commit()
        for filename in form.pictures.data:
            if str(filename).strip() == "":
                continue
            self.pictures.append(Picture(filename, self.id))
        commit()

    def remove_photos(self):
        for picture in self.pictures:
            remove_file(upload_set=photos, filename=f"{picture.filename}.jpeg")
            remove_file(upload_set=tif_photos, filename=f"{picture.filename}.tif")
        self.pictures = []


class FrontPage(BaseModel):
    __tablename__ = 'front_page'
    id = db.Column(db.Integer, primary_key=True)
    pictures = db.relationship("Picture")

    def store_photos(self, form):
        self.remove_photos()
        for filename in form.pictures.data:
            if str(filename).strip() == "":
                continue
            self.pictures.append(Picture(filename, front_page_id=self.id))
        commit("Failed to store front page photos")

    def remove_photos(self):
        for picture in self.pictures:
            remove_file(upload_set=photos, filename=f"{picture.filename}.jpeg")
            remove_file(upload_set=tif_photos, filename=f"{picture.filename}.tif")
        self.pictures = []


def remove_file(upload_set, filename):
    dir_path = upload_set.config.destination
    path = os.path.join(dir_path, filename)
    if os.path.exists(path=path):
        os.remove(path=path)
