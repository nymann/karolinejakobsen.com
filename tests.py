import unittest

from config import TestingConfig
from project import create_app


class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestingConfig)
        self.client = self.app.test_client()

    def test_front_page(self):
        with self.client:
            response = self.client.get('/')
            status_code = response.status_code
            self.assertTrue(status_code == 200)


if __name__ == '__main__':
    unittest.main()
